#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/LU>
#include <Eigen/QR>
#include <cmath>
#include <random>

using namespace std;
using Eigen::MatrixXf;
using Eigen::VectorXf;

int main()
{
    // заполнение входных данных
    VectorXf Z(12);
    Z << 27, 31, 43, 58, 69, 86, 102, 111, 122, 137, 18, 176;
    VectorXf T(12);
    T << 71, 64, 52, 41, 33, 23, 17, 12, 2, 0, 87, -5;

    MatrixXf AB_found(4,2); // найденные варианты А и В так как все неоднозначно работает

    /* Модель - линейная после логарифмирования:
     *        R0*10^K(T-T0)              1024*R0
     * Z=1024*------------- => lgZ = [ lg--------- - KT0 ] + KT => u=B+Ax
     *        Rc + R0                     Rc + R0
     * Решение в виде:
     *          T  ( A )    T
     * Fw=b => G G ( B ) = G y
     * G = T1 1   y = lgZ1
     *     T2 1       lgZ2
     *     T3 1       lgZ3
     * Невязка r = u-y = A*T + B - lgZ
     */

    // подготовка данных - очистка от выбросов
    /* Выберу 100 случайных троек уникальных пар
     * Для каждой тройки найду А и B
     * Для каждого A и B посчитаю невязки всех пар Z и T
     * Считаю суммарную невязку для куждого А и В
     * Нахожу минимальную среди них и считаю что это лучшая пара А и В
     * Убираю из расчетов все Z и T для которых невязка с этой парой больше чем 1,5 средней арифметической невязки
     * По оставшимся Z и T произвожу расчет рабочих А и В
     */

    MatrixXf best(2,Z.size()+1);
    best(1,Z.size()) = 100;//минимальная суммарная невязка
    MatrixXf G(3,2);
    G(1,0) = 1;
    G(1,1) = 1;
    G(2,1) = 1;
    VectorXf y(3);
    VectorXf AB(2);
    VectorXf tempT(12), tempZ(12), workT(12), workZ(12);
    int work_size = 0;

    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0,11);
    for (int i=0; i<100; i++)
    {
        G(0,0) = G(1,0) = G(2,0) = 0;
        int n = distribution(generator); // выбираю случаюную тройку разных пар
        G(0,0) = T(n);
        y(0) = log10(Z(n));
        do
        {
            n = distribution(generator);
            G(1,0) = T(n);
            y(1) = log10(Z(n));
        }
        while(G(0,0) == G(1,0));
        do
        {
            n = distribution(generator);
            G(2,0) = T(n);
            y(2) = log10(Z(n));
        }
        while(G(0,0) == G(2,0) || G(1,0) == G(2,0));
        AB = (G.transpose()*G).colPivHouseholderQr().solve(G.transpose()*y); // нахожу А и В
        best(0,Z.size()) = 0;
        for (int j=0; j<Z.size(); j++) // считаю суммарные невязки
        {
            best(0,j) = AB(0)*T(j)+AB(1) - log10(Z(j));
            best(0,Z.size()) +=abs(best(0,j));
        }
        if (best(0,Z.size())<=best(1,Z.size()))
        {
            AB_found(0,0)=AB(0);
            AB_found(0,1)=AB(1);
            for (int j=0;j<Z.size()+1;j++)
            {
                best(1,j) = best(0,j);
            }
        }
    }
    cout<<"Минимальная суммарная невязка при логарифмировании до искючения элементов "<<best(1,Z.size())<<endl;
    cout<<"Коэфиициенты при ней: А "<<AB_found(0,0)<<" B "<<AB_found(0,1)<<endl;
    best(1,Z.size()) /= Z.size();
    for (int i=0;i<Z.size();i++)
    {
        if (best(1,i)<best(1,Z.size())*1.5)
        {
            work_size++;
            tempZ(work_size-1) = Z(i);
            tempT(work_size-1) = T(i);
        }
    }
    workZ.resize(work_size);
    workT.resize(work_size);
    for (int i=0;i<work_size;i++)
    {
        workZ(i) = tempZ(i);
        workT(i) = tempT(i);
    }

    // расчет рабочих А и В
    /* Выберу 100 случайных троек уникальных пар
     * Для каждой тройки найду А и B
     * Для каждого A и B посчитаю невязки всех пар Z и T
     * Считаю суммарную невязку для куждого А и В
     * Нахожу минимальную среди них и считаю что это рабочие А и В
     */

    best.resize(1,3);
    best(0,2) = 100;
    std::uniform_int_distribution<int> distribution2(0,work_size-1); // не нашла как поменять диапазон
    for (int i=0; i<100; i++)
    {
        G(0,0) = G(1,0) = G(2,0) = 0;
        int n = distribution2(generator); // выбираю случаюную тройку разных пар
        G(0,0) = workT(n);
        y(0) = log10(workZ(n));
        do
        {
            n = distribution2(generator);
            G(1,0) = workT(n);
            y(1) = log10(workZ(n));
        }
        while(G(0,0) == G(1,0));
        do
        {
            n = distribution2(generator);
            G(2,0) = workT(n);
            y(2) = log10(workZ(n));
        }
        while(G(0,0) == G(2,0) || G(1,0) == G(2,0));
        AB = (G.transpose()*G).colPivHouseholderQr().solve(G.transpose()*y); // нахожу А и В
        float r_sum = 0;
        for (int j=0; j<workZ.size(); j++) // считаю суммарные невязки
        {
            r_sum +=abs(AB(0)*workT(j)+AB(1) - log10(workZ(j)));
        }
        if (r_sum<=best(0,2))
        {
            best(0,0) = AB(0);
            best(0,1) = AB(1);
            best(0,2) = r_sum;
        }
    }
    AB_found(1,0) = best(0,0);
    AB_found(1,1) = best(0,1);
    cout<<"Минимальная суммарная невязка при логарифмировании после исключения элементов "<<best(0,2)<<endl;
    cout<<"Коэфиициенты при ней: А "<<AB_found(1,0)<<" B "<<AB_found(1,1)<<endl;

    /* Модель - линейная:
     * Z = Ax+B
     * Решение в виде:
     *          T  ( A )    T
     * Fw=b => G G ( B ) = G y
     * G = T1 1   y = Z1
     *     T2 1       Z2
     *     T3 1       Z3
     * Невязка r = u-y = A*T + B - Z
     */

    // подготовка данных - очистка от выбросов
    /* Выберу 100 случайных троек уникальных пар
     * Для каждой тройки найду А и B
     * Для каждого A и B посчитаю невязки всех пар Z и T
     * Считаю суммарную невязку для куждого А и В
     * Нахожу минимальную среди них и считаю что это лучшая пара А и В
     * Убираю из расчетов все Z и T для которых невязка с этой парой больше чем 1,5 средней арифметической невязки
     * По оставшимся Z и T произвожу расчет рабочих А и В
     */

    best.resize(2,Z.size()+1);
    best(1,Z.size()) = 1000;//минимальная суммарная невязка
    workT.resize(12);
    workZ.resize(12);
    work_size = 0;

    std::uniform_int_distribution<int> distribution3(0,11);
    for (int i=0; i<100; i++)
    {
        G(0,0) = G(1,0) = G(2,0) = 0;
        int n = distribution3(generator); // выбираю случаюную тройку разных пар
        G(0,0) = T(n);
        y(0) = Z(n);
        do
        {
            n = distribution3(generator);
            G(1,0) = T(n);
            y(1) = Z(n);
        }
        while(G(0,0) == G(1,0));
        do
        {
            n = distribution3(generator);
            G(2,0) = T(n);
            y(2) = Z(n);
        }
        while(G(0,0) == G(2,0) || G(1,0) == G(2,0));
        AB = (G.transpose()*G).colPivHouseholderQr().solve(G.transpose()*y); // нахожу А и В
        best(0,Z.size()) = 0;
        for (int j=0; j<Z.size(); j++) // считаю суммарные невязки
        {
            best(0,j) = AB(0)*T(j)+AB(1) - Z(j);
            best(0,Z.size()) +=abs(best(0,j));
        }
        if (best(0,Z.size())<=best(1,Z.size()))
        {
            AB_found(2,0)=AB(0);
            AB_found(2,1)=AB(1);
            for (int j=0;j<Z.size()+1;j++)
            {
                best(1,j) = best(0,j);
            }
        }
    }
    cout<<"Минимальная суммарная невязка без логарифмирования до искючения элементов "<<best(1,Z.size())<<endl;
    cout<<"Коэфиициенты при ней: А "<<AB_found(2,0)<<" B "<<AB_found(2,1)<<endl;
    best(1,Z.size()) /= Z.size();
    for (int i=0;i<Z.size();i++)
    {
        if (best(1,i)<best(1,Z.size())*1.5)
        {
            work_size++;
            tempZ(work_size-1) = Z(i);
            tempT(work_size-1) = T(i);
        }
    }
    workZ.resize(work_size);
    workT.resize(work_size);
    for (int i=0;i<work_size;i++)
    {
        workZ(i) = tempZ(i);
        workT(i) = tempT(i);
    }

    // расчет рабочих А и В
    /* Выберу 100 случайных троек уникальных пар
     * Для каждой тройки найду А и B
     * Для каждого A и B посчитаю невязки всех пар Z и T
     * Считаю суммарную невязку для куждого А и В
     * Нахожу минимальную среди них и считаю что это рабочие А и В
     */

    best.resize(1,3);
    best(0,2) = 1000;
    std::uniform_int_distribution<int> distribution4(0,work_size-1); // не нашла как поменять диапазон
    for (int i=0; i<100; i++)
    {
        G(0,0) = G(1,0) = G(2,0) = 0;
        int n = distribution4(generator); // выбираю случаюную тройку разных пар
        G(0,0) = workT(n);
        y(0) = workZ(n);
        do
        {
            n = distribution4(generator);
            G(1,0) = workT(n);
            y(1) = workZ(n);
        }
        while(G(0,0) == G(1,0));
        do
        {
            n = distribution4(generator);
            G(2,0) = workT(n);
            y(2) = workZ(n);
        }
        while(G(0,0) == G(2,0) || G(1,0) == G(2,0));
        AB = (G.transpose()*G).colPivHouseholderQr().solve(G.transpose()*y); // нахожу А и В
        float r_sum = 0;
        for (int j=0; j<Z.size(); j++) // считаю суммарные невязки
        {
            r_sum +=abs(AB(0)*workT(j)+AB(1) - workZ(j));
        }
        if (r_sum<=best(0,2))
        {
            best(0,0) = AB(0);
            best(0,1) = AB(1);
            best(0,2) = r_sum;
        }
    }
    AB_found(3,0) = best(0,0);
    AB_found(3,1) = best(0,1);
    cout<<"Минимальная суммарная невязка без логарифмирования после исключения элементов "<<best(0,2)<<endl;
    cout<<"Коэфиициенты при ней: А "<<AB_found(3,0)<<" B "<<AB_found(3,1)<<endl;

    // расчет температуры
    /* T=(lgZ-B)/A
     */

    for (int i=0;i<Z.size();i++)
    {
        cout<<"Z = "<<Z(i)<<" T = "<<T(i)<<endl;
        cout<<"Модель с логарифмированием"<<endl;
        cout<<"Температура по коэффициентам до исключения элементов "<<(log10(Z(i))-AB_found(0,1))/AB_found(0,0)<<endl;
        cout<<"Невязка по коэффициентам до исключения элементов "<<AB_found(0,0)*T(i)+AB_found(0,1) - log10(Z(i))<<endl;
        cout<<"Температура по коэффициентам после исключения элементов "<<(log10(Z(i))-AB_found(1,1))/AB_found(1,0)<<endl;
        cout<<"Невязка по коэффициентам после исключения элементов "<<AB_found(1,0)*T(i)+AB_found(1,1) - log10(Z(i))<<endl;

        cout<<"Модель без логарифмирования"<<endl;
        cout<<"Температура по коэффициентам до исключения элементов "<<(Z(i)-AB_found(2,1))/AB_found(2,0)<<endl;
        cout<<"Невязка по коэффициентам до исключения элементов "<<AB_found(2,0)*T(i)+AB_found(2,1) - Z(i)<<endl;
        cout<<"Температура по коэффициентам после исключения элементов "<<(Z(i)-AB_found(3,1))/AB_found(3,0)<<endl;
        cout<<"Невязка по коэффициентам после исключения элементов "<<AB_found(3,0)*T(i)+AB_found(3,1) - Z(i)<<endl;
    }
    cout<<"Лучшая точность:"<<endl;
    cout<<"Модель с логарифмированием до исключения элементов 6\n"
          "Модель с логарифмированием после исключения элементов 0\n"
          "Модель без логарифмирования до исключения элементов 4\n"
          "Модель без логарифмирования после исключения элементов 3\n"<<endl;

    cout<<"Судя по невязкам проблема в конечных формулах, но я не знаю как их преобразовать\n"
          "Еще судя по результатам пересчет А и В после исключения элементов ухудшает точность"<<endl;

    cout<<"Введите показания индикатора Z"<< endl;
    float z;
    cin>>z;
    cout<<"Введите температуру Т для проверки точности"<<endl;
    float t;
    cin>>t;
    cout<<"Модель с логарифмированием"<<endl;
    cout<<"Температура по коэффициентам до исключения элементов "<<(log10(z)-AB_found(0,1))/AB_found(0,0)<<endl;
    cout<<"Невязка по коэффициентам до исключения элементов "<<AB_found(0,0)*t+AB_found(0,1) - log10(z)<<endl;
    cout<<"Температура по коэффициентам после исключения элементов "<<(log10(z)-AB_found(1,1))/AB_found(1,0)<<endl;
    cout<<"Невязка по коэффициентам после исключения элементов "<<AB_found(1,0)*t+AB_found(1,1) - log10(z)<<endl;

    cout<<"Модель без логарифмирования"<<endl;
    cout<<"Температура по коэффициентам до исключения элементов "<<(z-AB_found(2,1))/AB_found(2,0)<<endl;
    cout<<"Невязка по коэффициентам до исключения элементов "<<AB_found(2,0)*t+AB_found(2,1) - z<<endl;
    cout<<"Температура по коэффициентам после исключения элементов "<<(z-AB_found(3,1))/AB_found(3,0)<<endl;
    cout<<"Невязка по коэффициентам после исключения элементов "<<AB_found(3,0)*t+AB_found(3,1) - z<<endl;

    return 0;
}

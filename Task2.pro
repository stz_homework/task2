TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /usr/local/Cellar/eigen/3.3.7/include/eigen3

SOURCES += \
        main.cpp
